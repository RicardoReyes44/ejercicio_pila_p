'''
Created on 14 oct. 2020

@author: RSSpe
'''

from abc import ABC, abstractmethod


class RentaPeliculas(ABC):
    "" "Clase con operaciones basicas de pila" ""

    @abstractmethod
    def eliminar(self):
        pass


    @abstractmethod
    def agregar(self, pelicula):
        pass


    @abstractmethod
    def obtenerUltimo(self):
        pass


    @abstractmethod
    def obtenerTamaño(self):
        pass


    @abstractmethod
    def verificarPilaLlena(self):
        pass


    @abstractmethod
    def verificarPilaVacia(self):
        pass
