'''
Created on 14 oct. 2020

@author: RSSpe
'''

class Pelicula:
    "" "Clase para crear objetos de tipo Pelicula" ""

    def __init__(self, titulo, genero):
        self.titulo = titulo
        self.genero = genero

    def __str__(self):
        return f"Pelicula -> titulo=[{self.titulo}] genero=[{self.genero}]"
