'''
Created on 14 oct. 2020

@author: RSSpe
'''

from implementacion_pila_estatica import ImplementacionPilaEstatica
from pelicula import Pelicula


def main():
    "" "Funcion para comenzar el programa" ""

    ipe : ImplementacionPilaEstatica  = ImplementacionPilaEstatica()
    peliculasRentadas : Peliculas = []
    ultimo : int = -1
    carga : bool = False
    

    while True:
        print("1.- Cargar BD de peliculas\n"
        + "2.- Rentar pelicula\n"
        + "3.- Devolver pelicula\n"
        + "4.- Mostrar cantidad de peliculas disponibles para renta\n"
        + "5.- Salir\n")

        try:
            opcion : int = int(input("Introduce opcion: "))

            if opcion==1:
                ipe.agregar(Pelicula("Fnaf", "Terror"))
                ipe.agregar(Pelicula("Rocky VII", "Deporte"))
                ipe.agregar(Pelicula("Sonic The Hedgehog", "Aventura"))
                ipe.agregar(Pelicula("Venom", "Aventura"))
                ipe.agregar(Pelicula("Iron man", "Accion"))
                print("Base de datos cargada con exito")
                carga = True


            elif opcion==2:
                if ultimo==4:
                    print("No puedes dar mas peliculas de las que tienes\n\n")
                elif carga:
                    pelicula = ipe.eliminar()
                    peliculasRentadas.append(pelicula)
                    ultimo+=1
                    print(pelicula)
                else:
                    print("No se ha cargado la base de datos, por favor asegurate de cargarla primero\n\n")


            elif opcion==3:
                if ultimo==-1:
                    print("No puedes rentar mas peliculas de las que te pueden dar\n\n")
                elif carga:
                    print(ipe.agregar(peliculasRentadas[ultimo]))
                    peliculasRentadas.pop(ultimo)
                    ultimo-=1
                else:
                    print("No se ha cargado la base de datos, por favor asegurate de cargarla primero\n\n")


            elif opcion==4:
                if carga:
                    print("Cantidad de peliculas disponibles: ", ipe.obtenerTamaño())
                else:
                    print("No se ha cargado la base de datos, por favor asegurate de cargarla primero\n\n")


            elif opcion==5:
                print("\nPrograma terminado")
                break;


            else:
                print("No existe esa opcion, por favor prueba de nuevo")


        except ValueError as error:
            print("Error en la entrada de datos <", error ,">, por favor prueba de nuevo")

        print("Peliculas en el negocio: ", ipe)
        print("Peliculas rentadas: ", peliculasRentadas)
        print("------------------------------------------\n")


if __name__ == '__main__':
    main()
