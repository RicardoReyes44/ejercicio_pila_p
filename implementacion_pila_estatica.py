'''
Created on 14 oct. 2020

@author: RSSpe
'''

from renta_peliculas import RentaPeliculas
from pelicula import Pelicula


class ImplementacionPilaEstatica(RentaPeliculas):
    "" "Clase para crear e implementar nuestra pila" ""

    def __init__(self):
        self.peliculas : Pelicula = []
        self.ultimo : int = -1

    def agregar(self, pelicula) -> bool:
        "" "Metodo para agregar elementos" ""
        if not self.verificarPilaLlena():
            self.ultimo+=1
            self.peliculas.append(pelicula)
            return True
        else:
            return False


    def eliminar(self) -> Pelicula:
        "" "Metodo para eliminar ultimo elemento" ""
        if not self.verificarPilaVacia():
            self.ultimo-=1
            print()
            return self.peliculas.pop(len(self.peliculas)-1)
        else:
            return None


    def obtenerTamaño(self) -> int:
        "" "Metodo para obtener tamaño" ""
        return self.ultimo+1


    def obtenerUltimo(self) -> Pelicula:
        "" "Metodo para obtener ultimo elemento" ""
        if not self.verificarPilaVacia():
            return self.peliculas[self.ultimo]
        else:
            return None


    def verificarPilaLlena(self) -> bool:
        "" "Metodo para verificar si la pila esta llena" ""
        return self.ultimo==4


    def verificarPilaVacia(self):
        "" "Metodo para verificar si la pila esta vacia" ""
        return self.ultimo==-1


    def __str__(self):
        return f"ImplementacionPilaEstatica -> peliculas=[{self.peliculas}] ultimo=[{self.ultimo}]"
